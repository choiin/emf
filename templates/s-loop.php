<?php
$date_meta = str_replace('2020년 ','',get_field('publish')).(get_field('publish_end')?' ~ '.str_replace('2020년 ','',get_field('publish_end')):'');
if((int)(array_shift(get_the_terms(get_the_ID(),'work_years'))->slug) !== (int)get_field('main_years',3458)->slug) $date_meta = array_shift(get_the_terms(get_the_ID(),'work_years'))->slug;
?>
<a href="<?= get_permalink() ?>" title="<?= get_the_title() ?>">
    <div class="item" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)" alt="<?= get_field('thumbnail_image')['title'] ?>">
    <?php if(get_field('section_name')): ?>
        <div class="date">
                 <?= get_field('section_name') ?>
         </div>
    <?php elseif(!empty($date_meta)): ?><div class="date"><?= $date_meta ?></div><?php endif; ?>
    <div class="title"><?= get_the_title() ?></div>
    </div>
</a>