<a href="<?= get_permalink().$p ?>" title="<?= get_the_title() ?>">
    <div class="item" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)" alt="<?= get_field('thumbnail_image')['title'] ?>">
    <?php if(get_field('section_name')): ?><div class="date"><?= get_field('section_name') ?></div><?php endif; ?>
    <div class="title"><?= get_the_title() ?></div>
    </div>
</a>