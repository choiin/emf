<a href="<?= get_permalink() ?>" title="<?= get_the_title() ?>">
    <div class="item" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)" alt="<?= get_field('thumbnail_image')['title'] ?>">
    <?php if(get_field('section_name')): ?>
        <div class="date">
                 <?= get_field('section_name') ?>
         </div>
    <?php elseif(get_field('publish')): ?>
        <div class="date"><?= str_replace('2020년 ','',get_field('publish')).(get_field('publish_end')?' ~ '.str_replace('2020년 ','',get_field('publish_end')):'') ?></div><?php endif; ?>
    <div class="title"><?= get_the_title() ?></div>
    </div>
</a>