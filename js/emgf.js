//polyfill
if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
      'use strict';
      if (typeof start !== 'number') {
        start = 0;
      }
      
      if (start + search.length > this.length) {
        return false;
      } else {
        return this.indexOf(search, start) !== -1;
      }
    };
  }

var $ = jQuery.noConflict();
if(!$('body').hasClass('admin-bar')) SecureBlock();
$(window).load(function() {
    //if(location.href.includes('PageSpeed')) return;
});
$(document).ready(function() {
    Init();
});

$(window).scroll(function() {
    if($(window).scrollTop() > $('#floating_trigg').offset().top) {
        $('#floating_bar').css("display","block");
    } else {
        $('#floating_bar').css("display","none");
    }
});

function searchEvent() {
    var searchModal = $('.s_modal');
    searchModal.each(function(){
        $(this).on('click',function() {
            $('#s_ov').toggleClass('active');
            if($('#s_ov').hasClass('active')) $('.s_text')[0].select();
        });
    });

    $('#s_ov').on('click',function(){
        $(this).removeClass('active');
    });
    $('.s_text').on('click',function(){
        event.stopPropagation();
    });

    var searchBtn = $('.search_module .s_mit');
    searchBtn.each(function() {
        $(this).on('click',function(){
            DoSearch();
        });
    });
}

function isEng() {
    var ss = location.href.split('/');
    var iseng = false;

    $(ss).each(function() {
        if(this == 'en') iseng = true; 
    });
    return iseng;
}

function DoSearch() {
    var searchTxt = $('.s_text');

    var ss = location.href.split('/');
    var iseng = false;

    $(ss).each(function() {
        if(this == 'en') iseng = true; 
    });

    var res = '';
    if(iseng) {
        res += '/en';
    }
    if(searchTxt[0].value == '') {
        window.alert('검색어를 입력해주세요.');
        return;
    }
    res += '/?s='+searchTxt[0].value;
    location.href = location.origin + res;
}

function Init() {
    //SecureBlock();
    $('body').append($('#floating_bar'));

    var alert = document.createElement('div');
    alert.id="click_alert";
    $('body').append($(alert));

    UISetting();
    checkTime();
    searchEvent();
    if($('.owl-carousel')[0]) owlCaro();
    if($('.sc-video')[0]) VidEvent();
}
function MainArr(a,b) {
    var sel = 'owl-prev';
    if(b > 0) sel = 'owl-next';
    a.parent().parent().parent().find('.'+sel)[0].click();
}

function UISetting() {
    //dropdown
    var sel = $('.selectbox');
    sel.each(function() {
        $(this).on('click',function() {
            $(this).find('.sub-select').toggleClass('active');
        });

        var list = $(this).find('li');
        list.each(function() {
            $(this).on('click',function() {
                if($(this).parent().parent().find('.current') !== $(this).html()) {
                    $(this).parent().parent().find('.current').html($(this).html());
                    location.href=$(this).data('href');
                }
            });
        });
        // $(this).on('mouseout',function() {
        //     $(this).find('.sub-select').removeClass('active');
        // });
    });
}

function owlCaro() {
    $('.owl-carousel').owlCarousel({
        //center:true,
        loop:true,
        margin:15,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            980:{
                items:3
            }
        }
    });
}

function checkTime() {
    
    var timebar = $('.current-time');
    if(!timebar[0]) return;
    var cd = new Date();
    var week = ['일','월','화','수','목','금','토','일'];
    if(isEng()) week = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
    var data = cd.getFullYear() + '년 '+ ((cd.getMonth()+1)>12?1:(cd.getMonth()+1)) + '월 ' + cd.getDate() + '일 '+ week[cd.getDay()] +'요일<br>' + (cd.getHours()<13?'오전 '+numFormat(cd.getHours()):'오후 '+numFormat((cd.getHours()-12))) + ':' + numFormat(cd.getMinutes());
    if(isEng()) data = cd.getFullYear() + '.'+ ((cd.getMonth()+1)>12?1:(cd.getMonth()+1)) + '.' + cd.getDate() + ' '+ week[cd.getDay()] +' <br>' + (cd.getHours()<13?numFormat(cd.getHours()):numFormat((cd.getHours()-12))) + ':' + numFormat(cd.getMinutes()) + (cd.getHours()<13?' AM':' PM');
    timebar[0].innerHTML = data;

    setTimeout(checkTime,10000);
}

function numFormat(time) {
    return time<10?'0'+time:time;
}

var alertCor;

function Alert() {
    //window.alert('이화영상제의 모든 콘텐츠는 저작권자의 동의 없는 복제나 이용을 금합니다. 불법 다운로드, 녹화, 외부 유출/유포, 2차 가공 등의 저작권 침해 행위 발생 시 법적 조치하도록 하겠습니다.');
    $('#click_alert').addClass('active');
    clearTimeout(alertCor);
    alertCor = setTimeout(function() {
        $('#click_alert').removeClass('active');
    },5000);
}

function Nulling() {
        Alert();
        return false;
}

function SecureBlock() {
    $(window).on("contextmenu",Nulling);
    $(document).keydown(function(event) {
    if (event.keyCode == 123) { // Prevent F12
        Alert();
        return false;
    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
        Alert();
        return false;
    }
});
}

function ReleaseSec() {
    $(window).unbind("contextmenu",Nulling);
}

function fullScreenVid(a) {
    
    var vid = $(a).parent().parent();
    if(!vid[0]) return;
    vid.toggleClass('full-vid');
    if(vid.hasClass('full-vid')) $('body').append(vid);
    else $($('.screen-thumb')[vid.data('index')]).append(vid);
}
function playVid(a) {
    var vid = $(a).parent().parent().parent().find('video');

    if($(a).parent().parent().hasClass('paused')) vid[0].pause();
    else vid[0].play();

    $(a).parent().parent().toggleClass('paused');
}

var videvttime = [0,0];

function VisibleMeta(a) {
        a.find('.vid_meta').css({'opacity':1});
        target = a.data('index')?a.data('index'):0;
        clearTimeout(videvttime[target]);
        videvttime[target] = setTimeout(vidFade,2000,target);
}

function vidFade(a) {
    var target = $($('.sc-video .vid_meta')[a]);
    if(target.hasClass('paused')) target.css({'opacity':0});
}

function VidEvent() {
    $('video').each(function() {
        //$(this).attr('controls',false);
        //$(this).attr('autoplay',true);
    });
    $('.sc-video').each(function() {
        $(this).attr('data-index',$('.sc-video').index(this));
    

    $(this).on('mousemove',function(){VisibleMeta($(this))});
    $(this).find('.vid_meta').on('mousemove',function(){VisibleMeta($(this).parent());});

    });
}

function LocateEng(a) {
    event.preventDefault();
    if(a) {
        $('#trp-floater-ls-language-list a[title=English]')[0].click();
    }else{
        $('#trp-floater-ls-language-list a[title=Korean]')[0].click();
    }
}

function getContent(a) {
    var id = $(a).data('id');
    console.log(id);
    event.preventDefault();
    var cont = $('#archive_content');
    var data = {
        'action':'get_arc_content',
        'pid':id
    };
    $.post(sbiajaxurl,data).done(function(res) {
        cont.html(res);
    }).fail(function(res) {console.log(res);});
}

var currentModal = 0;
var maxModal = 0;

function CreateModal() {
    var modals = $('.site-style .et_pb_row:not(.modal_base)');
    maxModal = modals.length-1;
    modals.each(function() {
      $(this).addClass('modal_content');
      $('#main-content').append($(this));
    });

    var np = $('.modal-prev, .modal-next');
    np.each(function() {$('#main-content').append($(this));});
    $('.modal-prev').on('click',function() {
            //event.stopPropagation();
            var ind = (currentModal-1 < 0)?maxModal:currentModal-1;
            $('.site-style .site-list span[data-target='+ind+']').trigger('click');
            console.log(ind);
    });
    $('.modal-next').on('click',function() {
            //event.stopPropagation();
            var ind = (currentModal+1 > maxModal)?0:currentModal+1;
            $('.site-style .site-list span[data-target='+ind+']').trigger('click');
            console.log(ind);
    });
    $('.modal-exit').on('click',function() {
            $('.modal_content.active').removeClass('active');
            $('html').removeClass('disabled');
            $('.modal-prev, .modal-next, .modal-exit').each(function() {
                $(this).removeClass('active');
            });
    });
    var lists = $('.site-style .site-list span');
    lists.each(function() {
        $(this).attr('data-index',$(this)[0].innerHTML.split('.')[0]);
        $(this).on('click',function() {

            var m = $('.modal_content');
            m.each(function() {
                $(this).removeClass('active');
            });
            var index = $(this).data('target');
            currentModal = parseInt(index);
            $(m[index]).addClass('active');
            //$(m[index]).children('div').remove($('.modal-exit'));
            $(m[index]).children('div').prepend($('.modal-exit'));
            $('html').addClass('disabled');

            $('.modal-prev, .modal-next').each(function() {
                $(this).addClass('active');
            });



        });
    });
    var areas = $('.site-style area');
    areas.each(function() {
        $(this).on('click',function() {
            var ind = $(this).attr('title');
            $('.site-style .site-list span[data-index='+ind+']').trigger('click');
        });

        $(this).on('mouseover',function() {
            $('#modal-i').css({'top':event.clientY+30+'px','left':event.clientX+'px','display':'block'});
            var ind = $(this).attr('title');
            $('#modal-i').html($('.site-style .site-list span[data-index='+ind+']').html());
        });
        $(this).on('mouseout',function() {
            $('#modal-i').css({'display':'none'});
        })
    });
}

function setArchivePgnation() {
    var pgi = document.querySelectorAll('.wp-pagenavi a');
    var url = location.href.split('&pgi')[0];
    for(var i=0;i<pgi.length;i++) {
      var pgip = ParsePage(pgi[i].getAttribute('href'));
      pgi[i].setAttribute('href',url+'&pgi='+pgip); 
       }
}

function ParsePage(a) {
    var p = 1;
    if(a.includes('/page/')) p = a.split('/page/')[1].split('/')[0];
    return p;
}
    
