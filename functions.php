<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' ,10);
function my_theme_enqueue_styles() { 
    if(!is_admin()) {
    wp_enqueue_script( 'mutation', get_stylesheet_directory_uri() . '/js/MutationObserver.min.js', null, '2.2', 'mutation' );
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'style_less', get_stylesheet_directory_uri() . '/style.less', null, '2.2', 'screen' );
    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/js/jQuery.min.js', null, '2.2', 'jquery' );
    wp_enqueue_script( 'imageMaps', get_stylesheet_directory_uri() . '/js/imageMaps.min.js', null, '2.2', 'imageMaps' );
    wp_enqueue_script( 'emgf_js', get_stylesheet_directory_uri() . '/js/emgf.js', null, '2.2', 'emgf' );
    if(!current_user_can('administrator')) wp_enqueue_script( 'sec_js', get_stylesheet_directory_uri() . '/js/sec.js', null, '2.2', 'sec' );  
    }
}

function mainPageId() {
    return get_option('page_on_front');
}
function getMainYear() {
    return (int) get_field('main_years',mainPageId())->slug;
}

function disable_cptdivi()
{
	remove_action( 'wp_enqueue_scripts', 'et_divi_replace_stylesheet', 99999998 );
}
add_action('init', 'disable_cptdivi');//미친 레이아웃 제거
add_shortcode('taxloop','taxonomy_loop');
function taxonomy_loop() {

    $tax = 'work_category';
    if(!get_field('main_years')) return;
    $res='';
    $temp = get_stylesheet_directory() . '/templates/main-loop.php';
    $arg = array(
        'post_type'=>'egmf_work',
        'taxonomy'=>$tax,
        'hide_empty'=>true,
    );
    ob_start();
    $curyear = get_field('main_years')->slug;
    $items = !empty(get_field('main_list'))?get_field('main_list'):get_categories($arg);
    

    foreach($items as $item) {
        $cat = $item->name;
        $catid = $item->term_id;
        $parg = array(
            'post_type'=>'egmf_work',
            'post_status'=>'publish',
            'posts_per_page'=>16,
            //'paged'=>1,
            'tax_query'=> array(
                'relation'=>'AND',
                array(
                    'taxonomy'=>'work_years',
                    'field'=>'slug',
                    'terms'=>$curyear,
                    'operator'=>'IN'
                ),
                array(
                    'taxonomy'=>$tax,
                    'field'=>'term_id',
                    'terms'=>(int) $catid,
                    'operator'=>'IN'
                ),
            ),
        );
        $query = new WP_Query($parg);
        if($query->have_posts()):
            ?>
            <div class="et_pb_row cat_list" id="<?= $item->slug ?>">
            <div class="cat_header col-2">
                <div class="title"><a href="<?= get_term_link($item->term_id,$tax) ?>"><?= $item->name ?></a></div>
                <div class="cat_arr" style="text-align:right">
                    <button class="prev" onclick="MainArr($(this),-1)"></button>
                    <button class="next" onclick="MainArr($(this),1)"></button>
                </div>
            </div>
                <div class="owl-wrapper">
                    <div class="owl-carousel owl-theme">
                    <?php
                while($query->have_posts()):
                    $query->the_post();
                    load_template($temp,false);
                endwhile;
                ?>
                </div>
            </div>
            </div>
            <?php
            endif;
    }
    $res = ob_get_contents();
    if(empty($res)) {
        ?>
        <div class="ready">준비중입니다.</div>
        <?php
        $res = ob_get_contents();
    }
    ob_end_clean();
    return $res;
}

add_shortcode('main_archive_loop','main_archive_loop');
function main_archive_loop() {

    if(!get_field('main_years')) return;
    $curyear = get_field('main_years');
    $arg = array(
        'post_type'=>'egmf_work',
        'post_status'=>'publish',
        'posts_per_page'=>10,
        'orderby'=>'rand',
        'tax_query'=> array(
            'relation'=>'AND',
            array(
                'taxonomy'=>'work_years',
                'field'=>'slug',
                'terms'=>$curyear,
                'operator'=>'NOT IN'
            ),
        ),
    );
    $query = new WP_Query($arg);
    ob_start();
    if($query->have_posts()):
        ?>
        <div class="et_pb_row cat_list">
        <div class="cat_header col-2">
            <div class="title"><br></a></div>
            <div class="cat_arr" style="text-align:right">
                <button class="prev" onclick="MainArr($(this),-1)"></button>
                <button class="next" onclick="MainArr($(this),1)"></button>
            </div>
        </div>
            <div class="owl-wrapper">
                <div class="owl-carousel owl-theme">
                <?php
            while($query->have_posts()):
                $query->the_post();
                ?>
                <div class="item" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)" alt="<?= get_field('thumbnail_image')['title'] ?>">
                    <a href="<?= get_permalink() ?>" title="<?= get_the_title() ?>">
                    <?php if(array_shift(get_the_terms(get_the_ID(),'work_years'))->name): ?><div class="date"><?= array_shift(get_the_terms(get_the_ID(),'work_years'))->name ?></div><?php endif; ?>
                    <div class="title"><?= get_the_title() ?></div>
                    </a>
                </div>
                <?php
            endwhile;
            ?>
            </div>
        </div>
        </div>
        <?php
        endif;
    $res = ob_get_contents();
    ob_end_clean();
    return $res;
}

add_shortcode('translateMenu','translateMenu');
function translateMenu() {
    ob_start();

    ?>
    <div class="multi_lang">
        <?php if(!isEng()) { ?>
        <a href="#" onclick="event.preventDefault();$('#trp-floater-ls-language-list a[title=English]')[0].click();">EN</a>
        <?php } else { ?>
        <a href="#" onclick="event.preventDefault();$('#trp-floater-ls-language-list a[title=Korean]')[0].click();">KR</a>
        <?php } ?>
    </div>

    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('php','wpPhp');
function wpPhp($arg,$content = null) {
    $res = '';
    ob_start();
    eval($content);
    $res = ob_get_contents();
    ob_end_clean();
    return $res;
}

add_shortcode('search-result','searchResult');
function searchResult() {
    $out='';

    $curyear = get_field('main_years',mainPageId())->slug;

    $get = isset($_GET['s'])?$_GET['s']:'';
    $page = isset($_GET['paged'])?$_GET['paged']:get_query_var('paged');

    global $wp_query;
    $posts = array();


    $arg = array(
        //'s'=>$get,
        'post_type'=>'egmf_work',
        'post_status'=>'publish',
        'posts_per_page'=>16,
        'paged'=>$page?(int)$page:1,
        '_meta_or_title'=>$get,
        'tax_query'=>array(
            // array(
            //     'taxonomy'=>'work_years',
            //     'field'=>'slug',
            //     'terms'=>$curyear,
            //     'operator'=>'IN',
            // ),
            ),
        'meta_query'=>array(
            'relation'=>'OR',
            array(
                'key'=>'author',
                'value'=>$get,
                'compare'=>'LIKE',
            ),
            array(
                'key'=>'director_$_name',
                'value'=>$get,
                'compare'=>'LIKE',
            )
            ),
    );
    $query = new WP_Query($arg);
    ob_start();
    ?>
    <h1 class="search_header"><?= '\''.$get.'\'에 대한 검색결과.'?></h1>
    <div class="search_items category_items">
    <?php
    while($query->have_posts()):$query->the_post();
    load_template(get_stylesheet_directory().'/templates/s-loop.php',false);
    endwhile;
    ?>
    </div>
    <?php
    wp_pagenavi(array('query'=>$query));
    while($wp_query->have_posts()) {
        $wp_query->the_post();
        $posts[] = '\''.get_the_title().'\'';
    }
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_action( 'pre_get_posts', function( $q )
{
    if( $title = $q->get( '_meta_or_title' ) )
    {
        add_filter( 'get_meta_sql', function( $sql ) use ( $title )
        {
            global $wpdb;

            // Only run once:
            static $nr = 0; 
            if( 0 != $nr++ ) return $sql;

            // Modified WHERE
            $sql['where'] = sprintf(
                " AND ( %s OR %s ) ",
                $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title),
                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
            );

            return $sql;
        });
    }
});

function timetrans($seconds) {
    $t = round($seconds);
    $h = floor($t/3600);
    $m = $t/60%60;
    $s = $t%60;
    return ($h>0?$h.'h ':'').($m>0?$m.'m ':'').($s>0?$s.'s ':'');
  }

add_shortcode('catpage_loop','catpage_loop');
function catpage_loop() {
    $tax = 'work_category';

    $catid = isset($_GET[$tax])?$_GET[$tax]:get_queried_object()->slug;
    $p = isset($_GET['paged'])?$_GET['paged']:get_query_var('paged');
    $cat = get_term_by('slug',$catid,$tax);
    $out = '';
    $temp = get_stylesheet_directory().'/templates/category-list.php';

    $curyear = get_field('main_years',mainPageId())->slug;

    $arg = array(
        'post_type'=>'egmf_work',
        'post_status'=>'publish',
        'tax_query'=>array(
            'relation'=>'AND',
            array(
                'taxonomy'=>$tax,
                'field'=>'slug',
                'terms'=>$catid,
                'operator'=>'IN',
            ),
            array(
                'taxonomy'=>'work_years',
                'field'=>'slug',
                'terms'=>$curyear,
                'operator'=>'IN',

            ),
        ),
        'posts_per_page'=>16,
        'paged'=> (int)$p,
        'orderby'=>'post_date',
        'order'=>'DESC'
        );
    $query = new WP_Query($arg);
    ob_start();
    if($query->have_posts()):
        ?>
    <div class="category_items">
        <?php
    
    while($query->have_posts()):$query->the_post();
    load_template($temp,false);
    endwhile;
    ?>
    </div>
    <?php
    wp_pagenavi(array('query'=>$query));
endif;
    $out = ob_get_contents();
    ob_end_clean();


    return $out;
}
add_shortcode('venue_list','venue');
function venue() {
$arg = array(
	'post_type'=>'venue',
'post_status'=>'publish',
'posts_per_page'=>-1,
);
$query = new WP_Query($arg);
$out = '';

ob_start();
if($query->have_posts()):
echo '<ul>';
while($query->have_posts()):$query->the_post();
?>
<li class="item" data-id="<?= get_the_ID() ?>"><?= get_the_title() ?></li>
<?php
endwhile;
echo '</ul>';
$out = ob_get_contents();
endif;
return $out;

}


function getCurCatID($tax) {
    global $post;
    $postid = $post->ID;
    $cat = array_shift(get_the_terms($postid,$tax))->term_id;

    return $cat;
}
function getCurCat($postid,$tax) {
    $cat = array_shift(get_the_terms($postid,$tax));

    return $cat;
}

add_shortcode('getPrev','getPrev');
function getPrev() {
    $mainid = mainPageId();
    //$curyear = (array_shift(get_the_terms(get_the_ID(),'work_years'))->slug == get_field('main_years',$mainid)->slug)?(int) get_field('main_years',$mainid)->slug:(int)array_shift(get_the_terms(get_the_ID(),'work_years'))->slug;
    $thisyear = array_shift(get_the_terms(get_the_ID(),'work_years'))->slug;
    $curyear = get_field('main_years',$mainid)->slug;
    $iscurrent = ($curyear == $thisyear)?true:false;
    $cat = getCurCatID('work_category');
    $arg = array(
        'post_status'=>'publish',
        'post_type'=>'egmf_work',
        'tax_query'=>array(
            'relation'=>'AND',
            array(
                'taxonomy'=>'work_years',
                'field'=>'slug',
                'terms'=>$thisyear,
                //'operator'=>'IN'
            ),
        ),
        'posts_per_page'=>-1,
        'orderby'=>array(
            'post_date'=>'DESC',
            'post_title'=>'ASC',
        ),
    );
    if($iscurrent) {
        array_push($arg['tax_query'],array('taxonomy'=>'work_category','field'=>'term_id','terms'=>(int) $cat,));
    }
    $posts = get_posts($arg);
    $out = '';

    $ids = array();
    foreach($posts as $thepost) {
        $ids[] = $thepost->ID;
    }

    global $post;
    $thisindex = array_search( $post->ID, $ids );
    $previd    = isset( $ids[ $thisindex - 1 ] ) ? $ids[ $thisindex - 1 ] : 0;
    $nextid    = isset( $ids[ $thisindex + 1 ] ) ? $ids[ $thisindex + 1 ] : 0;

    ob_start();
    //echo $curyear.'/'.$thisyear;
    ?>
        <div class="pnpost">
    <?php
    if($previd > 0):
    ?>
    <div class="item">
        <a href="<?= get_permalink($previd) ?>">
        <div class="col-2 prev">
            <div class="title"><?= get_the_title($previd) ?></div>
            <div><?= $iscurrent?getCurCat($previd,'work_category')->name.'<br>':'' ?><?= get_field('publish',$previd)?get_field('publish',$previd):array_shift(get_the_terms($previd,'work_years'))->name ?></div>
        </div>
        </a>
    </div>
        <?php
    endif;
    if($nextid > 0):
        ?>
            <div class="item">

        <a href="<?= get_permalink($nextid) ?>">
        <div class="col-2 next">
        <div><?= $iscurrent?getCurCat($nextid,'work_category')->name.'<br>':'' ?><?= get_field('publish',$nextid)?get_field('publish',$nextid):array_shift(get_the_terms($nextid,'work_years'))->name ?></div>
            <div class="title"><?= get_the_title($nextid) ?></div>
        </div>
        </a>
    </div>
    <?php
    endif;
    ?>
    </div>
    <?php //endwrap
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
}

add_shortcode('add_video','add_video');
function add_video() {
    date_default_timezone_set('Asia/Seoul');

    $open = true;
    $curtime = time();//new DateTime(null, new DateTimeZone('Asia/Seoul'))
    $Ymd = str_replace(array('년 ','월 ','일'),'',get_field('publish'));
    $Ymde = str_replace(array('년 ','월 ','일'),'',get_field('publish_end'));
    $targettime = mktime('0','0','0',substr($Ymd, 4, 2),substr($Ymd, 6, 2),substr($Ymd, 0, 4));
    $endtime = mktime('0','0','0',substr($Ymde, 4, 2),substr($Ymde, 6, 2)+1,substr($Ymde, 0, 4));
    ob_start();
    //echo $curtime.'='.$targettime;
    //echo date("Y.m.d H:i:s",$endtime);
    if(((int)array_shift(get_the_terms(get_the_ID(),'work_years'))->slug == getMainYear())&&(!get_field('publish') || ($curtime < $targettime) || !$open)):
        ?>
        <div class="screen-thumb" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)">
            <!-- <div class="txt">
                <?= get_field('publish')?str_replace(getMainYear().'년 ','',get_field('publish')).' 공개':'상영 준비중 입니다.' ?>
            </div> -->
        </div>
        <?php
    elseif(get_field('images')):
    echo do_shortcode('[acf_gallery_carousel acf_field="images" autoplay="true" autoplay_speed="5000" media_size="full" slide_to_show="1"]');    
    elseif(((int)array_shift(get_the_terms(get_the_ID(),'work_years'))->slug == getMainYear())&&($curtime > $endtime) && get_field('publish_end') ):
        ?>
        <div class="screen-thumb" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)">
            <!-- <div class="txt">
                상영이 종료되었습니다.
            </div> -->
        </div>
        <?php
    else:
        if(get_field('video')):
        //echo do_shortcode('[video src="'.get_field('video').'"]');

        ?>
        <div class="screen-thumb">
            <div class="sc-video">
                <video src="<?= get_field('video') ?>"></video>
                <div class="vid_meta">
                    <div class="vid_play">
                        <!-- <div class="vid_prev" onclick="timeVid(-10)"></div> -->
                        <div class="vid_playbtn" onclick="playVid(this)"></div>
                        <!-- <div class="vid_next" onclick="timeVid(10)"></div> -->
                    </div>
                    <div class="fullscreen" onclick="fullScreenVid(this)"></div>
                </div>
            </div>
        </div>
        <?php
        elseif(get_field('video_embed')):
        echo get_field('video_embed');
        else:
            ?>
        <div class="screen-thumb" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)"></div>
            <?php
        endif;
    endif;
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('prog_meta','prog_meta');
function prog_meta() {
    ob_start();
    ?>
    <div class="metas">
    <table>
  <tbody>
<?php
$fields = array('author','director','duration','genre','year','publish_date','publish','section_name','script','screen_name');
foreach($fields as $field) {
$check=true;
$f = get_field_object($field);
if($f && !empty($f['value'])):
if($f['name'] == 'year'){$f['value']=array_shift(get_the_terms(get_the_ID(),'work_years'))->name;}
if($f['name'] == 'duration'){
    $t = (int)get_field('duration');
    $f['label']=(isEng()?'Running Time':'상영시간');
    $f['value']='';
    $f['value'] .= (!empty(get_field('duration')['h'])?get_field('duration')['h'].'h ':'');
    $f['value'] .= (!empty(get_field('duration')['m'])?get_field('duration')['m'].'min ':'');
    $f['value'] .= (!empty(get_field('duration')['s'])?get_field('duration')['s'].'sec':'');
}
if($f['name'] == 'publish') {
    $f['label'] = (isEng()?'Screening Schedule':'상영일');
    $f['value'] .= (get_field('publish_end')?' '.(isEng()?'~':'부터').'<br> '.get_field('publish_end').(isEng()?'':' 까지'):'');
    if(get_field('publish_date')) {
        $f['label'] = '';
        $f['value'] = '';
    }
}
if($f['name'] == 'author') {
    $tax = array_shift(get_the_terms(get_the_ID(),'work_category'))->name;
    switch(get_field('job')['value']) {
        case 'Artist':$f['label']='작가명';break;
        case 'Product':$f['label']='제작';break;
        default:$f['label']='감독명';
    }
    //2020년 카테고리 별 직업보정
    if(array_shift(get_the_terms(get_the_ID(),'work_years'))->slug == '2020'){
        if(strpos($tax,'EMAP') !== false) $f['label'] = isEng()?'Artist':'작가명';
        else if(strpos($tax,'EMF') !== false) $f['label'] = isEng()?'Production':'제작';
        else $f['label'] = isEng()?'Director':'감독';
    }
}
if($f['name'] == 'director') {
    $tax = array_shift(get_the_terms(get_the_ID(),'work_category'))->name;
    $names = $f['name'];
    switch(get_field('job')['value']) {
        case 'Artist':$f['label']='작가명';break;
        case 'Product':$f['label']='제작';break;
        default:$f['label']='감독명';
    }
    if(have_rows($names)):
        $out=array();
        while(have_rows($names)):the_row();
            $out[] = get_sub_field('name');
        endwhile;
        $f['value']=implode(',',$out);
        if(empty($f['value'])) $check=false;
    endif;
    //2020년 카테고리 별 직업보정
    if(array_shift(get_the_terms(get_the_ID(),'work_years'))->slug == '2020'){
        if(strpos($tax,'EMAP') !== false) $f['label'] = isEng()?'Artist':'작가명';
        else if(strpos($tax,'EMF') !== false) $f['label'] = isEng()?'Production':'제작';
        else $f['label'] = isEng()?'Director':'감독';
        }
    }

    if($check && !empty($f['value'])):
    echo '<tr>';
    echo '<td>'.$f['label'].'</td>';
    echo '<td>'.$f['value'].'</td>';
    echo '</tr>';
    endif;
endif;
}
?>
    </tbody>
</table>
</div>
<?php
$dist = get_field('distribute');
$dist_empty = true;
foreach($dist as $dis) {
    if($dis) $dist_empty = false;
}
if(!$dist_empty): ?>
<div class="distributor">
<p>상영본 제공처</p>
<table>
<tbody>
<?php $dist_arr = get_field_object('distribute')['sub_fields']; foreach($dist_arr as $dis):if(get_field('distribute_'.$dis['name'])): ?>
    <tr>
    <td><?= $dis['label'] ?></td>
    <td><?= get_field('distribute_'.$dis['name']) ?></td>
    </tr>
<?php endif;endforeach;?>
</tbody>
</table>
</div>
<?php endif;?>
<!--제공처-->
<?php
$out = ob_get_contents();
ob_end_clean();
return isset($out)?$out:'';
}

add_shortcode('directors','directors');
function directors() {
    ob_start();
    if(have_rows('director')):
        while(have_rows('director')):the_row();
    ?>
    <div class="director_wrap multi">

    <?php if(get_sub_field('photo')): ?><div class="photo"><div><div style="background-image:url(<?= get_sub_field('photo') ?>)"></div></div></div><?php endif; ?>
        <div class="desc <?= (get_sub_field('photo'))?'':'full' ?>">
        <p><?= get_sub_field('name') ?></p>
        <p><?= get_sub_field('desc') ?></p>
        </div>
    </div>
    <?php
        endwhile;
        elseif(get_field('author')):
    ?>
    <div class="director_wrap single">

    <?php if(get_field('author_photo')): ?><div class="photo"><div><div style="background-image:url(<?= get_field('author_photo') ?>)"></div></div></div><?php endif; ?>
        <div class="desc <?= (get_field('author_photo'))?'':'full' ?>">
        <p><?= get_field('author') ?></p>
        <p><?= get_field('director_info') ?></p>
    </div>
    </div>
    <?php
        endif;
    $out = ob_get_contents();
    ob_end_clean();

    if(!empty($out)) $out = '<h2 style="margin-bottom:1rem">'.director_title().'</h2>'.$out;

    return $out;
}

//프로그램 뒤로가기
add_shortcode('tax_link','tax_link');
function tax_link() {
    $taxname = 'work_category';
    $tax = array_shift(get_the_terms(get_the_ID(),$taxname));

    $taxid = $tax->term_id;
    $taxlink = get_term_link($taxid,$taxname);
    $tax_title = $tax->name;

    if(($slug = array_shift(get_the_terms(get_the_ID(),'work_years'))->slug) !== get_field('main_years',mainPageId())->slug):
    $term = get_term_by('slug',$slug,'work_years');
    $arg = array(
        'post_type'=>'egmf_archive',
        'post_status'=>'publish',
        'meta_query'=>array(
            array(
                'key'=>'years',
                'value'=>$term->term_id,
                'compare'=>'LIKE',
            ),
        ),
    );
    $query = new WP_Query($arg);
    $taxlink=get_permalink($query->posts[0]->ID).'?work_years='.$slug;
    $tax_title=$query->posts[0]->post_title;
    endif;

    $st = "'".$tax_title."'(으)로 돌아가기.";
    if(isEng()) $st = "Back to '".$tax_title."'";

    $back_query = isset($_GET['pgi'])?'&pgi='.$_GET['pgi']:'';

    ob_start();
    ?>
    <div class="tax_link_wrap">
            <a href="<?= $taxlink.$back_query ?>">
                <div class="title"><?= $st ?></div>
            </a>
    </div>

    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;

}

//커뮤니티 뒤로가기
add_shortcode('board_link','board_link');
function board_link() {
    $taxname = 'board_category';
    $tax = array_shift(get_the_terms(get_the_ID(),$taxname));
    
    $taxid = $tax->term_id;
    $taxlink = get_term_link($taxid,$taxname);
    ob_start();
    ?>
    <div class="tax_link_wrap">
            <a href="<?= $taxlink ?>">
                <div class="title">'<?= $tax->name ?>'(으)로 돌아가기.</div>
            </a>
    </div>

    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;

}
add_shortcode('getPostPrev','getPostPrev');
function getPostPrev() {
    $cat = getCurCatID('board_category');
    $arg = array(
        'post_status'=>'publish',
        'post_type'=>'board_posts',
        'tax_query'=>array(
            'relation'=>'AND',
            array(
                'taxonomy'=>'board_category',
                'field'=>'term_id',
                'terms'=>(int) $cat,
            ),
        ),
        'posts_per_page'=>-1,
        'orderby'=>'publish_date',
        'order'=>'DESC'
    );
    $posts = get_posts($arg);
    $out = '';

    $ids = array();
    foreach($posts as $thepost) {
        $ids[] = $thepost->ID;
    }

    global $post;
    $thisindex = array_search( $post->ID, $ids );
    $previd    = isset( $ids[ $thisindex - 1 ] ) ? $ids[ $thisindex - 1 ] : 0;
    $nextid    = isset( $ids[ $thisindex + 1 ] ) ? $ids[ $thisindex + 1 ] : 0;

    ob_start();
    //echo $cat;
    ?>
        <div class="pnpost">
    <?php
    if($previd > 0):
    ?>
    <div class="item">
        <a href="<?= get_permalink($previd) ?>">
        <div class="col-2 prev">
            <div class="title"><?= get_the_title($previd) ?></div>
            <div><?= get_the_date('Y년 m월 d일',$previd) ?></div>
        </div>
        </a>
    </div>
        <?php
    endif;
    if($nextid > 0):
        ?>
            <div class="item">

        <a href="<?= get_permalink($nextid) ?>">
        <div class="col-2 next">
            <div><?= get_the_date('Y년 m월 d일',$nextid) ?></div>
            <div class="title"><?= get_the_title($nextid) ?></div>
        </div>
        </a>
    </div>
    <?php
    endif;
    ?>
    </div>
    <?php //endwrap
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
}


add_shortcode('director_title','director_title');
function director_title() {
    $tax = array_shift(get_the_terms(get_the_ID(),'work_category'))->name;
    switch(get_field('job')['value']) {
        case 'Artist':$out='ARTIST/작가';break;
        case 'Product':$out='PRODUCTION/제작';break;
        default:$out='DIRECTOR/감독';
    }
    if(array_shift(get_the_terms(get_the_ID(),'work_years'))->slug == '2020') $out = (strpos($tax,'EFF') !== false)?'DIRECTOR/감독':(((strpos($tax,'EMF') !== false)?'PRODUCTION/제작':'ARTIST/작가'));
    return $out;
}

add_shortcode('drop_year','drop_year');
function drop_year() {
    $arg = array(
        'post_type'=>'egmf_work',
        'taxonomy'=>'work_years',
        'hide_empty'=>true,
        'orderby'=>'name',
        'order'=>'DESC'
    );
    $taxs = get_categories($arg);
    $curyear = isset($_GET[$tax])?$_GET[$tax]:get_queried_object()->slug;
    ob_start();
    ?>
    <div class="selectbox">
        <span class="current"><?= !empty($curyear)?$curyear:(getMainYear() - 1) ?></span>
        <ul class="sub-select">
            <?php foreach($taxs as $tax):if((int)$tax->name !== getMainYear()): ?>
                <li data-href="<?= get_term_link($tax->term_id,'work_years') ?>"><?= $tax->name ?></li>
            <?php endif;endforeach; ?>
        </ul>
    </div>

    <?php

    $out=ob_get_contents();
    ob_end_clean();
    return $out;
}

//add_shortcode('archive_loop','archive_loop');
function archive_loop($year) {
    $tax = 'work_years';

    $catid = isset($_GET[$tax])?$_GET[$tax]:get_queried_object()->slug;
    $p = isset($_GET['pgi'])?$_GET['pgi']:get_query_var('paged');
    $out = '';
    $temp = get_stylesheet_directory().'/templates/archive-list.php';

    $curyear = $year;
    $arg = array(
        'post_type'=>'egmf_work',
        'post_status'=>'publish',
        'tax_query'=>array(
            'relation'=>'AND',
            array(
                'taxonomy'=>'work_years',
                'field'=>'slug',
                'terms'=>$curyear,
                'operator'=>'IN',

            ),
        ),
        'posts_per_page'=>16,
        'paged'=> (int)$p,
        'orderby'=>array(
            'post_date'=>'DESC',
            'post_title'=>'ASC',
        ),
        );
    $query = new WP_Query($arg);
    ob_start();
    if($query->have_posts()):
        ?>
    <div class="category_items">
        <?php
    
    while($query->have_posts()):$query->the_post();
    ?>
        <a href="<?= get_permalink().'?pgi='.$p ?>" title="<?= get_the_title() ?>">
            <div class="item" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)" alt="<?= get_field('thumbnail_image')['title'] ?>">
            <?php if(get_field('section_name')): ?><div class="date"><?= get_field('section_name') ?></div><?php endif; ?>
            <div class="title"><?= get_the_title() ?></div>
            </div>
        </a>
    <?php
    //load_template($temp,false);
    endwhile;
    ?>
    </div>
    <?php
    wp_pagenavi(array('query'=>$query));
endif;
    $out = ob_get_contents();
    ob_end_clean();


    return $out;
}

//언론보도
add_shortcode('board_loop','board_loop');
function board_loop($atts) {

	$tax = isset($_GET[$tax])?$_GET[$tax]:get_queried_object()->slug;

    $p = isset($_GET['paged'])?$_GET['paged']:get_query_var('paged');

    $arg = array(
        'post_type'=>'board_posts',
        'post_status'=>'publish',
        'tax_query'=>array(
            'relation'=>'AND',
            array(
                'taxonomy'=>'board_category',
                'field'=>'slug',
                'terms'=>$tax,
                'operator'=>'IN',
            ),
        ),
        'posts_per_page'=>16,
        'paged'=> (int)$p,
        'orderby'=>'post_date',
        'order'=>'DESC'
        );

    if($tax == 'news') {
        $arg['orderby'] = 'order_news';
        $arg['order']= 'DESC';
        $arg['meta_query'] = array(
            'order_news'=>array(
                'key'=>'news_date',
            ),
        );
    }
    $query = new WP_Query($arg);

    $link = '';
    $meta = '';
    $ptype = '';

    ob_start();
    if($query->have_posts()):
        ?>
    <div class="category_items">
        <?php
    if($tax !== 'news'):
    while($query->have_posts()):$query->the_post();
    $link = get_permalink();
    switch($tax) {
        case 'news': $link = get_field('news_link');$meta = get_field('news_date');$ptype='news';break;
        case 'video': $meta = get_field('video_year');break;
        case 'daily_news': $meta = get_the_date("Y년 m월 d일");break;
    }
        ?>
            <a href="<?= $link ?>?paged=<?= $p ?>" title="<?= get_the_title() ?>">
                <div class="item" style="background-image:url(<?= get_field('thumbnail_image')['sizes']['large'] ?>)" alt="<?= get_field('thumbnail_image')['title'] ?>">
                <?php if(!empty($meta)): ?><div class="date"><?= $meta ?></div><?php endif; ?>
                <div class="title"><?= get_the_title() ?></div>
                </div>
            </a>
        <?php
    endwhile;
    elseif($tax=='news'):
        while($query->have_posts()):$query->the_post();
        ?>
            <a href="<?= get_field('news_link') ?>" title="<?= get_the_title() ?>" target="_blank">
                <div class="item news">
                    <div class="ntitle"><?= get_the_title() ?></div>
                    <div class="ndesc"><?= get_field('news_excerpt') ?></div>
                    <div class="nmeta">
                        <div><?= get_field('news_date') ?></div>
                        <div><?= get_field('news_from')?'<div>'.get_field('news_from').'</div>':'' ?></div>
                    </div>

                </div>
            </a>
        <?php
        endwhile;
    endif;
    ?>
    </div>
    <?php
    wp_pagenavi(array('query'=>$query));
endif;
    $out = ob_get_contents();
    ob_end_clean();


    return $out;
}

add_shortcode('board_video','board_video');
function board_video() {
    $names = 'board_video';
    ob_start();
        while(have_rows($names)):the_row();
            if(get_sub_field('upload')):
            echo do_shortcode('[video src="'.get_sub_field('upload').'"]');
            endif;
            if(get_sub_field('embed')):
            echo get_sub_field('embed');
            endif;
        endwhile;
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

//divi
add_filter('et_pb_module_shortcode_attributes', 'dbcf_sort_gallery_by_name', 10, 3);
add_filter('et_module_shortcode_output', 'dbcf_clear_gallery_sorting');

	function dbcf_sort_gallery_by_name($props, $atts, $slug) {
		if ($slug === 'et_pb_gallery') {
			add_action('pre_get_posts', 'dbcf_gallery_orderby_date');
		}
		return $props;
	}

	function dbcf_clear_gallery_sorting($content) {
		remove_action('pre_get_posts', 'dbcf_gallery_orderby_date');
		return $content;
	}

	function dbcf_gallery_orderby_date($query) {	
		//$query->set('orderby', 'date');
		$query->set('order', 'DESC');
    }


function isEng() {
    $iseng = false;
    $cururl = explode('/',$_SERVER['REQUEST_URI']);
    foreach($cururl as $url) {
        if($url == 'en') $iseng = true;
    }
    return $iseng;
}
    
add_shortcode('multi_link','multi_link');
function multi_link() {
    ob_start();
    if(isEng()):
    ?>
    <a href="#" onclick="LocateEng(false)">KR</a>
    <?php
    else:
    ?>
    <a href="#" onclick="LocateEng(true)">ENG</a>
    <?php
    endif;
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
}

add_shortcode('archive_head','archive_head');
function archive_head() {
    ob_start();
    ?>
    <div class="archive_head tax_meta">
    <div>
    <div><h1>Archive</h1></div>
    <div></div>
    <div><h1><?= get_the_title(); ?></h1></div>
    </div>
    </div>
    <?php
    $out=ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('archive_tabs','archive_tabs');
function archive_tabs() {
    $f = 'archive_page';
    $i=0;
    $pid = isset($_GET['pid'])?$_GET['pid']:0;
    ob_start();
    ?>
    <div class="archive_tabs">
    <ul class="archive_tab et_pb_tabs_controls">
        <?php while(have_rows($f)):the_row(); ?>
        <li class="<?= (($i == $pid) && (!isset($_GET['work_years'])))?'et_pb_tab_active':'' ?>"><a href="<?= get_permalink().'?pid='.$i ?>"><?= get_sub_field('title') ?></a></li>
        <?php $i++;endwhile; ?>
        <?php if(get_field('years')): ?>
            <li class="<?= isset($_GET['work_years'])?'et_pb_tab_active':'' ?>" onclick=""><a href="<?= get_permalink().'?work_years='.get_field('years')->slug.'&pgi=1' ?>">프로그램</a></li>
        <?php endif;?>
    </ul>
    </div>
    <?php
    $out=ob_get_contents();
    
    ob_end_clean();
    return $out;
}

add_shortcode('archive_content','archive_content');
function archive_content() {
    $quer = isset($_GET['pid'])?(int)$_GET['pid']:0;
    $i = 0;
    ob_start();
    if(isset($_GET['work_years'])):
        echo archive_loop($_GET['work_years']);
    else:
        while(have_rows('archive_page')):the_row();
        if($i==$quer) {
            echo '<div class="'.get_sub_field('wide').'">';
            echo do_shortcode(get_post_field('post_content', get_sub_field('page')->ID));
            echo '</div>';
        }
        $i++;
        endwhile;
    endif;
    $out=ob_get_contents();
    ob_end_clean();
    return $out;
}

add_action('wp_ajax_get_arc_content','get_arc_content');
function get_arc_content() {
    $pid = (int) $_POST['pid'];
    $out = apply_filters('post_content',get_post_field('post_content', $pid));
    //$out = '[archive_tabs]';
    echo do_shortcode($out);
    wp_die();
}
